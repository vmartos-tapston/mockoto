const Optional = (function () {
    return {
        of: function (value) {
            let done = false;   //calling 'then' or 'else' more than once has no effect
            function exists(value) {
                return value !== undefined && value !== null;
            }
            return {
                then: function (func) {
                    if (exists(value) && !done) {
                        done = true;
                        func(value);
                    }
                    return this;
                },
                else: function (func) {
                    if (!exists(value) && !done) {
                        done = true;
                        func();
                    }
                    return this;
                },
                map: function (func) {
                    if (exists(value)) {
                        return Optional.of(func(value));
                    }
                    return this;
                }
            }
        }
    }
}());
Optional.EMPTY = Object.freeze(Optional.of(null));

module.exports = Optional;