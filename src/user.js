const F = require('./F');

const User = (function() {
    const USER = {
        id: 8004,
        name: "Vadim",
        roles: ["user", "root"]
    };

    function copy(obj) {
        return Object.assign({}, obj);
    }

    return {
        get: function(id) {
            return USER.id === id ? copy(USER) : undefined;
        },
        authenticate: function(user) {
            return (user.name === USER.name && user.password === 'secret') ? copy(USER) : undefined;
        }
    }
}());


module.exports = User;