const F = require('./F');

function setJson(obj) {
    return function(response) {
        return response.json(obj);
    }
}

function setStatus(status) {
    return function(response) {
        return response.status(status);
    }
}

function setHeader({key, value}) {
    return function(response) {
        return response.set(key, value);
    }
}

function setHeaders(headers) {
    return function(response) {
        headers.forEach(h => setHeader(h)(response));
        return response;
    }
}

function doNothing() {
    return F.identity;
}

function done(response) {
    return response.end();
}

function send(responseObj) {
    const withStatus = F.compose(F.ifElse(F.valuable, setStatus, doNothing), F.prop('status'))(responseObj);
    const withBody = F.compose(F.ifElse(F.valuable, setJson, doNothing), F.prop('body'))(responseObj);
    const withHeaders = F.compose(F.ifElse(F.valuable, setHeaders, doNothing), F.prop('headers'))(responseObj);
    return {
        to: F.compose(done, withHeaders, withBody, withStatus)
    };
}

function getHeader(name, request) {
    return request.get(name);
}

const Http = {
    send: send,
    getHeader: F.curry(getHeader)
};

module.exports = Http;