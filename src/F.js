function curry(fn) {
    const arity = fn.length;
    return function f1(...f1Args) {
        if (f1Args.length >= arity) {
            return fn(...f1Args);
        }
        return function f2(...f2Args) {
            const finalArgs = f1Args.concat(f2Args);
            return f1(...finalArgs);
        }
    }
}

function compose(...fns) {
    return function (...args) {
        return fns.reduceRight(apply, ...args);
    }
}

function pipe(...fns) {
    return function (...args) {
        return fns.reduce(apply, ...args);
    }
}

function prop(name, object) {
    return valuable(object) ? object[name] : object;
}

function setProp(name, value, object) {
    const result = Object.assign({}, object);
    result[name] = value;
    return result;
}

function apply(object, fn) {
    return fn(object);
}

function ifElse(condition, thenFn, elseFn) {
    return function (...args) {
        return condition(...args) ? thenFn(...args) : elseFn(...args);
    }
}

function when(condition, thenFn) {
    return function (...args) {
        return condition(...args) ? thenFn(...args) : identity(...args);
    }
}

function unless(condition, elseFn) {
    return function (...args) {
        return condition(...args) ? identity(...args) : elseFn(...args);
    }
}

function valuable(value) {
    return value !== undefined && value !== null;
}

function identity(value) {
    return value;
}

function optional(value) {
    function some() {
        return {
            map: fn => optional(fn(value)),
            chain: fn => fn(value),
            orElse: x => value
        }
    }

    function nothing() {
        return {
            map: fn => nothing(),
            chain: fn => undefined,
            orElse: x => x
        }
    }

    return valuable(value) ? some() : nothing();
}

function catchable(...functions) {

    function then(fn) {
        const fns = functions.slice();
        fns.push(fn);
        return catchable(...fns);
    }

    function done(onResult, onError) {
        return function (...args) {
            try {
                const result = functions.reduce(apply, ...args);
                return onResult(result);
            } catch (e) {
                return onError(e);
            }
        }
    }

    return {
        then: then,
        done: done
    }

    // function value(v) {
    //     return {
    //         map: (fn1) => catchable(fn1)(v),
    //         fold: (onValue, onError) => onValue(v)
    //     }
    // }
    //
    // function error(e) {
    //     return {
    //         map: () => error(e),
    //         fold: (onValue, onError) => onError(e)
    //     }
    // }
    //
    // function does(...args) {
    //     try {
    //         return value(fn(...args));
    //     } catch (e) {
    //         return error(e);
    //     }
    // }
}

const F = {
    curry: curry,
    compose: curry(compose),
    pipe: curry(pipe),
    prop: curry(prop),
    setProp: curry(setProp),
    apply: curry(apply),
    ifElse: curry(ifElse),
    when: curry(when),
    unless: curry(unless),
    valuable: curry(valuable),
    identity: curry(identity),
    optional: optional,
    catchable: catchable,
};

module.exports = F;