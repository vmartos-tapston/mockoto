const express = require('express');
const bodyParser = require('body-parser');
const User = require('./user');
const Authorization = require('./authorization');
const F = require('./F');
const Http = require('./http');

const app = express();
app.use(bodyParser.json());

app.post('/auth', function (request, response) {
    const getToken = F.compose(Authorization.generateToken, User.authenticate, F.prop('body'));
    const getResponse = (token) => F.optional(token).map(Authorization.authorized).orElse(Authorization.unauthorized());
    const authorize = F.compose(getResponse, getToken);
    Http.send(authorize(request)).to(response);
});

app.get('/user', function (request, response) {
    const getPayload = F.compose(Authorization.validate, Http.getHeader(Authorization.AUTH_HEADER));
    const getUser = F.compose(User.get, F.prop('id'));
    const getUserResponse = function (user) {
        return {status: 200, body: user}
    };
    const getResponse = F.catchable(getPayload).then(getUser).done(getUserResponse, Authorization.unauthorized);
    Http.send(getResponse(request)).to(response);
});

const server = app.listen(8081, function () {
    console.log("Mockoto works at http://localhost:%s", server.address().port);
});