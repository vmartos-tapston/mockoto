const jwt = require('jsonwebtoken');
const F = require("./F");

const JWT_SECRET = "tapston-secret";
const AUTH_HEADER = "Authorization";
const EXPIRES_IN = 5 * 60;  //5 minutes

function validate(token) {
    return jwt.verify(token, JWT_SECRET);
}

function generateToken(user) {
    return jwt.sign({roles: user.roles, id: user.id}, JWT_SECRET, {expiresIn: EXPIRES_IN});
}

function authorized(token) {
    return {
        headers: [{key: AUTH_HEADER, value: token}],
        status: 200
    }
}

function unauthorized(e) {
    const msg = e && e.message;
    return {
        status: 401,
        body: {msg: msg}
    }
}

const Authorization = {
    validate: validate,
    generateToken: F.when(F.valuable, generateToken),
    authorized: authorized,
    unauthorized: unauthorized,
    AUTH_HEADER
};

module.exports = Authorization;